import './style.css';

async function fetchIntro() {
  const introResponse = await fetch("/api/intro");
  const intro = await introResponse.json();
  
  const name = document.querySelector<HTMLHeadingElement>(".name")!;
  const description = document.querySelector<HTMLParagraphElement>(".description")!;
  const contacts = document.querySelector<HTMLParagraphElement>(".contacts")!;
  
  name.innerHTML = intro.name;
  description.innerHTML = intro.description;
  contacts.innerHTML = `You can contact me at ${intro.contacts.map(({label, url}: {label: string, url: string}) => `<a class="text-blue-300 underline" href="${url}">${label}</a>`).join(", ")}`
}

fetchIntro();