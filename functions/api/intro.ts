export const onRequestGet = async({request, url}) => {
    return new Response(JSON.stringify({
        name: "Selvaraj Antonyraj",
        description: "Hello 👋  &nbsp; I'm <b>Selva</b>. I'm a Frontend Engineer who is passionate about developing accessible and performant websites. I have been in this field for 11+ years",
        contacts: [{
            "label": "Twitter",
            "url": "https://twitter.com/selvaa89"
        }, {
            "label": "Linkedin",
            "url": "https://www.linkedin.com/in/selva-raj-97b0033a/"
        }]
    }), {
        headers: {
            "content-type": "application/json"
        }
    })
};